import logging
import grpc


import news_classifier_pb2, news_classifier_pb2_grpc

shared_folder = '/home/slakshmana/dataset'

def run_classifier():
    logging.info("Calling NewsClassifier_Stub...")

    with grpc.insecure_channel('localhost:8062') as channel:
        classifier_stub = news_classifier_pb2_grpc.NewsClassifierStub(channel)


        request_train = news_classifier_pb2.TrainingConfig(
                                                    training_data_filename = shared_folder+'/reuters_training_data.npz',
                                                    training_labels_filename = shared_folder+'/reuters_training_labels.npz',
                                                    epochs = 9,
                                                    batch_size = 512,
                                                    validation_ratio = 0.1,
                                                    model_filename = shared_folder+'/reuters_model')

  
        response_train = classifier_stub.startTraining(request_train)

        request_classify = news_classifier_pb2.NewsText(text='This is a news classifier. Please enter the newwires here. You can then find their category!')
        response_classify = classifier_stub.classify(request_classify)
        
        return response_train, response_classify


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    response_train,  response_classify = run_classifier()
   