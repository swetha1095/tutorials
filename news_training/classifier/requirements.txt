grpcio==1.38.0
grpcio-tools==1.38.0
grpc-interceptor
multithreading
protobuf==3.16.0
numpy
tensorflow
keras2onnx
tf2onnx

Bootstrap-Flask==1.8.0
Flask==2.0.2
idna==3.3
Jinja2==3.0.3
MarkupSafe==2.0.1
python-dateutil==2.8.2
Werkzeug==2.0.2
Flask-WTF==0.14.3
WTForms==2.3.3

