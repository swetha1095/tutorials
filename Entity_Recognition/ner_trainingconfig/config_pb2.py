# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: config.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='config.proto',
  package='',
  syntax='proto3',
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x0c\x63onfig.proto\"\x07\n\x05\x45mpty\"\x1e\n\tEmbedding\x12\x11\n\tembedding\x18\x01 \x01(\t\"\xe1\x01\n\x0eTrainingConfig\x12\x1e\n\x16training_data_filename\x18\x01 \x01(\t\x12 \n\x18validation_data_filename\x18\x02 \x01(\t\x12\x1a\n\x12test_data_filename\x18\x03 \x01(\t\x12\x1e\n\nembeddings\x18\x04 \x03(\x0b\x32\n.Embedding\x12\x0e\n\x06\x65pochs\x18\x05 \x01(\x05\x12\x15\n\rlearning_rate\x18\x06 \x01(\x02\x12\x12\n\nbatch_size\x18\x07 \x01(\x05\x12\x16\n\x0emodel_filename\x18\x08 \x01(\t2A\n\x15TrainingConfiguration\x12(\n\rstartTraining\x12\x06.Empty\x1a\x0f.TrainingConfigb\x06proto3'
)




_EMPTY = _descriptor.Descriptor(
  name='Empty',
  full_name='Empty',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=16,
  serialized_end=23,
)


_EMBEDDING = _descriptor.Descriptor(
  name='Embedding',
  full_name='Embedding',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='embedding', full_name='Embedding.embedding', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=25,
  serialized_end=55,
)


_TRAININGCONFIG = _descriptor.Descriptor(
  name='TrainingConfig',
  full_name='TrainingConfig',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='training_data_filename', full_name='TrainingConfig.training_data_filename', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='validation_data_filename', full_name='TrainingConfig.validation_data_filename', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='test_data_filename', full_name='TrainingConfig.test_data_filename', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='embeddings', full_name='TrainingConfig.embeddings', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='epochs', full_name='TrainingConfig.epochs', index=4,
      number=5, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='learning_rate', full_name='TrainingConfig.learning_rate', index=5,
      number=6, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='batch_size', full_name='TrainingConfig.batch_size', index=6,
      number=7, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='model_filename', full_name='TrainingConfig.model_filename', index=7,
      number=8, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=58,
  serialized_end=283,
)

_TRAININGCONFIG.fields_by_name['embeddings'].message_type = _EMBEDDING
DESCRIPTOR.message_types_by_name['Empty'] = _EMPTY
DESCRIPTOR.message_types_by_name['Embedding'] = _EMBEDDING
DESCRIPTOR.message_types_by_name['TrainingConfig'] = _TRAININGCONFIG
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Empty = _reflection.GeneratedProtocolMessageType('Empty', (_message.Message,), {
  'DESCRIPTOR' : _EMPTY,
  '__module__' : 'config_pb2'
  # @@protoc_insertion_point(class_scope:Empty)
  })
_sym_db.RegisterMessage(Empty)

Embedding = _reflection.GeneratedProtocolMessageType('Embedding', (_message.Message,), {
  'DESCRIPTOR' : _EMBEDDING,
  '__module__' : 'config_pb2'
  # @@protoc_insertion_point(class_scope:Embedding)
  })
_sym_db.RegisterMessage(Embedding)

TrainingConfig = _reflection.GeneratedProtocolMessageType('TrainingConfig', (_message.Message,), {
  'DESCRIPTOR' : _TRAININGCONFIG,
  '__module__' : 'config_pb2'
  # @@protoc_insertion_point(class_scope:TrainingConfig)
  })
_sym_db.RegisterMessage(TrainingConfig)



_TRAININGCONFIGURATION = _descriptor.ServiceDescriptor(
  name='TrainingConfiguration',
  full_name='TrainingConfiguration',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=285,
  serialized_end=350,
  methods=[
  _descriptor.MethodDescriptor(
    name='startTraining',
    full_name='TrainingConfiguration.startTraining',
    index=0,
    containing_service=None,
    input_type=_EMPTY,
    output_type=_TRAININGCONFIG,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_TRAININGCONFIGURATION)

DESCRIPTOR.services_by_name['TrainingConfiguration'] = _TRAININGCONFIGURATION

# @@protoc_insertion_point(module_scope)
