import logging
from timeit import default_timer as timer
import grpc
import config_pb2
import config_pb2_grpc


def run():
    logging.basicConfig()
    print("Calling HPP_Stub..")
    start_ch = timer()
    with grpc.insecure_channel('localhost:8061') as channel:
        stub = config_pb2_grpc.TrainingConfigurationStub(channel)
        ui_request = config_pb2.Empty()
        print("-------------- Receive input --------------")
        response = stub.startTraining(ui_request)

    print(response.status_text)
    end_ch = timer()
    print('Done!')
    ch_time = end_ch - start_ch
    print('Time for connecting to server = {}'.format(ch_time))

    return response.text


if __name__ == '__main__':
    logging.basicConfig()
    run()
