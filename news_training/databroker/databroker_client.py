import logging
import grpc

import news_databroker_pb2, news_databroker_pb2_grpc

def run_ID():
    logging.debug("Calling Databroker_Stub - NewsText...")
    with grpc.insecure_channel('localhost:10061') as channel:
        stub_ID = news_databroker_pb2_grpc.NewsDatabrokerStub(channel)
        request_message = news_databroker_pb2.NewsText(text='The news will be printed here.')
        print(request_message)
        response = stub_ID.get_next(request_message)
        print(response)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    response_ID = run_ID()
    