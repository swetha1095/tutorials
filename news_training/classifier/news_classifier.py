import grpc
from concurrent import futures
import time
import news_classifier_pb2
import news_classifier_pb2_grpc
import os
import numpy as np
import tensorflow.keras
from tensorflow.keras import layers
from tensorflow.keras import callbacks
from tensorflow.keras.datasets import reuters
from tensorflow.keras.preprocessing import sequence
import tf2onnx.convert
import onnx
import threading
from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, ValidationError
from wtforms.fields import StringField, IntegerField, FloatField, SubmitField

import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

port = 8061
shared_folder = os.getenv("SHARED_FOLDER_PATH")
tensorflow.random.set_seed(1234) # Set the seed here

# 46 labels for the Reuters dataset
rev_topic_map = {'copper': 6, 'livestock': 28, 'gold': 25, 'money-fx': 19, 'ipi (Industrial Production Index)': 30, 'trade': 11, 'cocoa': 0, 'iron-steel': 31, 
                 'reserves': 12, 'tin': 26, 'zinc': 37, 'jobs': 34, 'ship': 13, 'cotton': 14, 'alum': 23, 'strategic-metal': 27, 'lead': 45, 'housing': 7, 
                 'meal-feed': 22, 'gnp (Gross National Product)': 21, 'sugar': 10, 'rubber': 32, 'dlr (dollar)': 40, 'veg-oil': 2, 'interest': 20, 'crude': 16, 
                 'coffee': 9, 'wheat': 5, 'carcass': 15, 'lei (Legal Entity Identifier)': 35, 'gas': 41, 'nat-gas': 17, 'oilseed': 24, 'orange': 38, 'heat': 33, 
                 'wpi (Wholesale Price Index)': 43, 'silver': 42, 'cpi (Consumer Price Index)': 18, 'earn': 3, 'bop (Balance of Payments)': 36, 'money-supply': 8, 
                 'hog (Harley-Davidson Owners Group)': 44, 'acq (acquisition)': 4, 'pet-chem': 39, 'grain': 1, 'retail': 29}

# Note down the maximum no. of word indices present in the dataset
topic_map = dict([(value, key) for key, value in rev_topic_map.items()])
word_index = reuters.get_word_index()
sorted_word_index = sorted(word_index.items())
sorted_word_index_key, sorted_word_index_value = zip(*sorted_word_index) 

tensorboard_callback = callbacks.TensorBoard(log_dir=shared_folder)
app = Flask(__name__)


def vectorize_sequences(sequences, dimension=max(sorted_word_index_value)):
    results = np.zeros((len(sequences), dimension))
    for i, sequence in enumerate(sequences):
        results[i, sequence] = 1.
    return results


class ClassifierInputForm(FlaskForm):
    NewsText = StringField('NewsText', validators=[DataRequired()])
    Run = SubmitField('Run Classify')


@app.route('/', methods=['GET', 'POST'])
def classifier_input():
    form = ClassifierInputForm()
    print("classifier_input()")
    if form.Run.data and form.validate_on_submit():
        print("Processing user inputs")
        request = news_classifier_pb2.NewsText()
        request.text = form.NewsText.data
        result = classifier.classify(request, None)
        print(f"classify result: {result.category_text}")
    else:
        result=news_classifier_pb2.NewsCategory()
        result.category_text="None"
        result.category_code=0

    sorted_labels_dict = {k: v for k, v in sorted(rev_topic_map.items(), key=lambda item: item[1])} # Sort the 46 labels and display them on the UI
    return render_template("index.html", example_form=form, result=result, list_labels=sorted_labels_dict)


def app_run():
    app.secret_key = "hpp"
    bootstrap = Bootstrap(app)
    app.run(host="0.0.0.0", port=8062)


class NewsClassifier(news_classifier_pb2_grpc.NewsClassifierServicer):

    def __init__(self):
        self.create_model()

    def create_model(self):
        self.model = tensorflow.keras.Sequential([
            layers.Dense(64, activation='relu'),
            layers.Dense(64, activation='relu'),
            layers.Dense(46, activation='softmax')
        ])
        self.model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

    def startTraining(self, request, context):
        print(f"start training with request:\n{request}")
        self.create_model()
        x_train = np.load(request.training_data_filename)['arr_0']
        y_labels = np.load(request.training_labels_filename)['arr_0']
        print(f"data loading completed x_train:{len(x_train)} y_labels:{len(y_labels)}")
        split_border = int(len(x_train) * request.validation_ratio)
        print(f"split border: {split_border}")
        x_val = x_train[:split_border]
        partial_x_train = x_train[split_border:]
        y_val = y_labels[:split_border]
        partial_y_train = y_labels[split_border:]

        print("start model.fit()")
        history = self.model.fit(x=partial_x_train, y=partial_y_train, epochs=request.epochs,
                                 batch_size=request.batch_size, validation_data=(x_val, y_val), callbacks=[tensorboard_callback])
        print(f"training finished with accuracy={history.history['accuracy'][-1]} and validation_loss={history.history['val_loss'][-1]}")
        self.model.save(request.model_filename + '.h5')
        print(f"model is available here: {request.model_filename + '.h5'}")

        # Convert the model to onnx format

        tf2onnx.convert.from_keras(self.model, output_path=request.model_filename + '.onnx')
       
        # Check if the converted onnx model is valid or not
        try:
            onnx.checker.check_model(request.model_filename + '.onnx')
        except onnx.checker.ValidationError as e:
            print("The converted onnx model is invalid: %s" % e)
        else:
            print("The converted onnx model is valid!")
            print(f"model is available here: {request.model_filename + '.onnx'}")

        response = news_classifier_pb2.TrainingStatus()
        response.accuracy = history.history['accuracy'][-1]
        response.validation_loss = history.history['val_loss'][-1]
        response.status_text = 'success'
        return response

    def classify(self, request, context):
        response = news_classifier_pb2.NewsCategory()
        maxlen = max(sorted_word_index_value)
        words = request.text.split()
        news_indexed = []
        for word in words:
            if word not in word_index:
                news_indexed.append(2)
            else:
                news_indexed.append(word_index[word] + 3)

        news_padded = sequence.pad_sequences([news_indexed], truncating='pre', padding='pre', maxlen=maxlen)
        news_padded = vectorize_sequences(news_padded, dimension=max(sorted_word_index_value)) # Vectorize the test sequences

        prediction = self.model.predict(news_padded)
        response.category_code=np.argmax(prediction)
        response.category_text=topic_map[response.category_code]
        return response

server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
classifier = NewsClassifier()
news_classifier_pb2_grpc.add_NewsClassifierServicer_to_server(classifier, server)
print("Starting server. Listening on port : " + str(port))
server.add_insecure_port("[::]:{}".format(port))
server.start()
print("Start classifier web-ui")
threading.Thread(target=app_run()).start()
server.wait_for_termination()
